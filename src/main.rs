extern crate glutin_window;
extern crate graphics;
extern crate opengl_graphics;
extern crate piston;

use glutin_window::GlutinWindow as Window;
use opengl_graphics::{GlGraphics, OpenGL};
use piston::event_loop::*;
use piston::input::*;
use piston::window::WindowSettings;

pub struct App {
    gl: GlGraphics, // OpenGL drawing handle
    player_ship: PlayerShip
}

impl App {
    fn render(&mut self, args: &RenderArgs) {
        use graphics::*;

        const WHITE: [f32; 4] = [1.0, 1.0, 1.0, 1.0];
        self.gl.draw(args.viewport(), |c, gl| {
            clear(WHITE, gl);
        });

        self.player_ship.render(&mut self.gl, args);
    }

    fn update(&mut self, args: &UpdateArgs) {
        self.player_ship.update();
    }

    fn handle_input(&mut self, args: &ButtonArgs) {
        if let Button::Keyboard(k) = args.button {
            let velocity = match k {
                Key::Left => -1.0,
                Key::Right => 1.0,
                _ => 0.0,
            };
            match args.state {
                ButtonState::Press => self.player_ship.add_velocity(velocity),
                ButtonState::Release => self.player_ship.add_velocity(-velocity),
            }
        }
    }
}

struct PlayerShip {
    x: f64,
    y: f64,
    width: f64,
    height: f64,
    velocity: f64,
}

impl PlayerShip {
    fn render(&mut self, gl: &mut GlGraphics, args: &RenderArgs) {
        use graphics::*;
        const BLACK: [f32; 4] = [0.0, 0.0, 0.0, 1.0];

        gl.draw(args.viewport(), |c, gl| {
            let transform = c.transform;
            rectangle(BLACK, [self.x, self.y, self.width, self.height], transform, gl);
        });
    }

    fn update(&mut self) {
        self.x += self.velocity;
    }

    fn add_velocity(&mut self, velocity: f64) {
        let sum = self.velocity + velocity;
        if sum >= -1.0 && sum <= 1.0 {
            self.velocity = sum;
        }
    }
}

fn main() {
    let opengl = OpenGL::V3_2;
    let mut window: Window = WindowSettings::new("Pozaziemscy zaborcy", (800, 600))
        .opengl(opengl)
        .exit_on_esc(true)
        .build()
        .unwrap();

    let mut app = App {
        gl: GlGraphics::new(opengl),
        player_ship: PlayerShip { x: 350.0, y: 500.0, width: 50.0, height: 10.0, velocity: 0.0 }
    };

    let mut events = Events::new(EventSettings::new()).ups(60); // For now, we limit to 60 FPS to keep things simple
    // "while let" and "if let" are things that exectute only if pattern matches
    // Relevant chapter in the book https://doc.rust-lang.org/stable/book/second-edition/ch06-03-if-let.html
    // In the line below we basically say: do a while loop if events.next() returns Some. The actual event is moved into variable "e" (event)
    while let Some(e) = events.next(&mut window) {
        if let Some(r) = e.render_args() {
            app.render(&r);
        }

        if let Some(u) = e.update_args() {
            app.update(&u);
        }

        if let Some(b) = e.button_args() {
            app.handle_input(&b);
        }
    }
}
